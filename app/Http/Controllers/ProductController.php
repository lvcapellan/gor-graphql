<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     //
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     //
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     //
    // }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    // public function show(Product $product)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    // public function edit(Product $product)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, Product $product)
    // {
    //     //
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    // public function destroy(Product $product)
    // {
    //     //
    // }

    public function upload(Request $request)
    {
        if($request->sync_type == 1 && $request->date_uploaded == null)
        {
            $newProduct = new Product;

            $newProduct->store_id               = $request->store_id;
            $newProduct->sku_id                 = $request->sku_id;
            $newProduct->sync_type              = 0;
            $newProduct->date_uploaded          = $request->date_uploaded;
            $newProduct->sync_publish_unpublish = $request->sync_publish_unpublish;
            $newProduct->for_delete             = $request->for_delete;
            $newProduct->sync_for_image         = $request->sync_for_image;
            $newProduct->publish_status         = $request->publish_status;

            $newProduct->save();
        } else {
            $updateProduct = Product::where('sku_id', $request->sku_id);

            $updateProduct->store_id               = $request->store_id;
            $updateProduct->sku_id                 = $request->sku_id;
            $updateProduct->sync_type              = $request->sync_type;
            $updateProduct->date_uploaded          = $request->date_uploaded;
            $updateProduct->sync_publish_unpublish = $request->sync_publish_unpublish;
            $updateProduct->for_delete             = $request->for_delete;
            $updateProduct->sync_for_image         = $request->sync_for_image;
            $updateProduct->publish_status         = $request->publish_status;

            $updateProduct->save();
        }
    }

    public function delete(Request $request)
    {
        if($request->for_delete == 1){
            $deleteProduct = Product::where('sku_id', $request->sku_id);

            //call Shopify API

            $deleteProduct->delete();
        }
    }

    public function publishStatus(Request $request)
    {
        $product = Product::where('sku_id', $request->sku_id);

        if($request->sync_publish_unpublish == 1 && $request->publish_status == 1){
            $product->publish_status = 1;
        } elseif($request->sync_publish_unpublish == 1 && $request->publish_status == 0){
            $product->publish_status = 0;
        } elseif($request->sync_publish_unpublish == 2 && $request->publish_status == null){
            $product->publish_status = 0;
        } elseif ($request->sync_publish_unpublish == 3 && $request->publish_status == null) {
            $product->publish_status = 1;
        }

        $product->save();
    }

    public function imageUpload(Request $request)
    {
        if($request->sync_for_image == 1){
            // call internal API
        }
    }
}
